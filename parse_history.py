#!/bin/python

import re
import string
import os


def add_athletes_totals(data):
	nbr_rows = len(data)
	ath_id = -1
	name = "default name"
	i = 0
	total_temp = {}
	while i < nbr_rows:
		if data[i].find("on Strava</title>") > 0:
			name = data[i].split(">")[1].split("|")[0]
		if data[i].find("https://www.strava.com/athletes/") > 0:
			ath_id = data[i].split('?')[0].split('/athletes/')[1] # get last one, should be just the id value
		if data[i].find("row athlete-records") > 0:
			temp = {}
			temp[0] = ath_id
			temp[1] = name
			l = str(data[i+7]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp[2] = k[1][0]
			total_temp[0] = temp
			# i + 14 = distance (miles)
			#<td>1,749.2<abbr class='unit' title='miles'>mi</abbr></td>
			l = str(data[i+14]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[1][0].replace(',','')
			temp[1] = k[2][0]
			total_temp[1] = temp
			# i + 18 = time (hour,minute)
			#<td>120<abbr class='unit' title='hour'>h</abbr> 38<abbr class='unit' title='minute'>m</abbr></td>
			l = str(data[i+18]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[1][0].replace(',','')
			temp[1] = k[3][0]
			total_temp[2] = temp
			# i + 22 = elevation gain (feet)
			#<td>110,335<abbr class='unit' title='feet'>ft</abbr></td>
			l = str(data[i+22]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[1][0].replace(',','')
			temp[1] = k[2][0]
			total_temp[3] = temp
			# i + 26 = nbr of rides
			#<td>72</td>
			l = str(data[i+26]).split('>')[1].split('<')[0]
			total_temp[4] = l
			break
		i = i+1
	return total_temp	

def month_totals(data):
	nbr_rows = len(data)
	ath_id = -1
	name = "default name"
	i = 0
	month_temp = {}
	while i < nbr_rows:
		if data[i].find("on Strava</title>") > 0:
			name = data[i].split(">")[1].split("|")[0]
		if data[i].find("https://www.strava.com/athletes/") > 0:
			ath_id = data[i].split('?')[0].split('/athletes/')[1] # get last one, should be just the id value
		if data[i].find("Current Month") > 0:
			month_temp[0] = [ath_id,name]
			# i + 4 = distance
			l = str(data[i+4]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[0][0].replace(',','')
			temp[1] = k[1][0]
			month_temp[1] = temp
			# i + 9 = time
			l = str(data[i+9]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[1][0].replace(',','')
			temp[1] = k[3][0]
			month_temp[2] = temp
			# i + 13 = elevation
			l = str(data[i+13]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[1][0].replace(',','')
			temp[1] = k[2][0]
			month_temp[3] = temp
		i = i+1

	return month_temp

def main():
	ytd = {} # {ath_id,distance,time,elevation,rides}
	alltime = {}
	month = {}

	#f = open('/Users/mlonergan/mkl_test/mkl.html','r')
	#data = f.readlines()

	#month[0] = month_totals(data)

	n = 0
	for i in os.listdir(os.getcwd()):
		if i.endswith(".html"):
			f = open(i, 'r')
			data = f.readlines()
			ytd[n] = add_athletes_totals(data)
			month[n] = month_totals(data)
			n = n+1
	#print '-------------------------'
	#print month
	print '-------------------------'
	#print ytd
	for i in ytd:
		print ytd[i]
	print '-------------------------'
	#for i in month:
	#	print month[i]
	f.close()

main()
