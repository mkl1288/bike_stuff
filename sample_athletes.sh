#!/bin/sh


declare -A ath_ids

counter=0
while [ $counter -lt 10 ]; do
 #r=$((RANDOM%3000000+200))
 r=$(od -vAn -N4 -tu < /dev/urandom)
 let r=r*$RANDOM
 let r=r%6000000+200
 ath_ids[$counter]=$r
 let counter=counter+1
done


echo ${ath_ids[*]}
echo ------------------------

for id in ${ath_ids[*]}
 do
	curl https://www.strava.com/athletes/$id > $id.html
done

python ~/bike_stuff/parse_history.py


for id in ${ath_ids[*]}
 do
	rm $id.html
done
