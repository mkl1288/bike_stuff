#!/bin/sh

ath_id="942173"
curl https://www.strava.com/athletes/$ath_id > $ath_id.html
python ~/mkl_test/elevation_progress.py $ath_id
rm $ath_id.html

ath_id="408720"
curl https://www.strava.com/athletes/$ath_id > $ath_id.html
python ~/mkl_test/elevation_progress.py $ath_id
rm $ath_id.html
