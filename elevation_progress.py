#!/bin/python

import re
import string
import os
import datetime
import sys


def add_athletes_totals(data):
	nbr_rows = len(data)
	ath_id = -1
	i = 0
	total_temp = {}
	while i < nbr_rows:
		if data[i].find("https://www.strava.com/athletes/") > 0:
			ath_id = data[i].split('?')[0].split('/athletes/')[1] # get last one, should be just the id value
		if data[i].find("row athlete-records") > 0:
			temp = {}
			temp[0] = ath_id
			l = str(data[i+7]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp[1] = k[1][0]
			total_temp[0] = temp
			# i + 14 = distance (miles)
			#<td>1,749.2<abbr class='unit' title='miles'>mi</abbr></td>
			l = str(data[i+14]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[1][0].replace(',','')
			temp[1] = k[2][0]
			total_temp[1] = temp
			# i + 18 = time (hour,minute)
			#<td>120<abbr class='unit' title='hour'>h</abbr> 38<abbr class='unit' title='minute'>m</abbr></td>
			l = str(data[i+18]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[1][0].replace(',','')
			temp[1] = k[3][0]
			total_temp[2] = temp
			# i + 22 = elevation gain (feet)
			#<td>110,335<abbr class='unit' title='feet'>ft</abbr></td>
			l = str(data[i+22]).split('>')
			j = 0
			k = {}
			while j < len(l):
				k[j] = l[j].split('<')
				j = j+1
			temp = {}
			temp[0] = k[1][0].replace(',','')
			temp[1] = k[2][0]
			total_temp[3] = temp
			# i + 26 = nbr of rides
			#<td>72</td>
			l = str(data[i+26]).split('>')[1].split('<')[0]
			total_temp[4] = l
			break
		i = i+1
	return total_temp	

def elevation_progress(goal, ytd):
	# get day diff (31-dec) - today
	end = datetime.date(2015,12,31)
	today = datetime.date.today()
	days_left_a = end - today
	days_left = days_left_a.days
	days_past = 365 - days_left
	actual = int(ytd[3][0])

	print 'Current Elevation: ' + str(actual)

	# get linear progress required
	dist_left = goal - actual
	weeks_left = days_left / 7
	weekly_requirement = (dist_left+weeks_left-1) / weeks_left # add weeks_left-1 to round up instead of the default integer division
	print 'Weekly elevation required: ' + str(weekly_requirement)

	# get past linear progress
	weeks_past = 52 - weeks_left
	weekly_performance = actual / weeks_past # get lower bound to be conservative
	print 'YTD weekly elevation: ' + str(weekly_performance)

def elevation_progress_modified(goal, nbr_planned_weeks_off, known_future_dist, ytd):
	# get day diff (31-dec) - today
	end = datetime.date(2015,12,31)
	today = datetime.date.today()
	days_left_a = end - today
	days_left = days_left_a.days
	days_past = 365 - days_left
	actual = int(ytd[3][0])

	# get linear progress required
	dist_left = goal - actual - known_future_dist # subtract any known dist that may throw off weekly averages
	weeks_left = (days_left / 7)
	actual_weeks_left = weeks_left - nbr_planned_weeks_off
	weekly_requirement = (dist_left+actual_weeks_left-1) / actual_weeks_left # add actual_weeks_left-1 to round up instead of the default integer division

	# get past linear progress
	weeks_past = 52 - weeks_left
	weekly_performance = actual / weeks_past # get lower bound to be conservative

	temp = {} # {actual,needed,weeks_left,planned_weeks_off,past,dist_already_counted,required}
	temp[0] = actual
	temp[1] = dist_left
	temp[2] = weeks_left
	temp[3] = nbr_planned_weeks_off
	temp[4] = weekly_performance
	temp[5] = known_future_dist
	temp[6] = weekly_requirement

	return temp

def main():
	ath_id = sys.argv[1]
	ytd = {} # {ath_id,distance,time,elevation,rides}

	#print '------------------------------------------------'
	#print os.getcwd()
	#print '------------------------------------------------'
	f = open(os.getcwd() + '/' + str(ath_id) + '.html','r')
	#f = open('/Users/mlonergan/mkl_test/sample_files/mkl.html','r')
	data = f.readlines()

	print 'Athlete: ' + str(ath_id)

	ytd = add_athletes_totals(data)

	# get user goal, nbr_planned_weeks_off
	n=0
	print_flag = 0
	while n < 100000 and print_flag == 1:
		print '------------------------------------------'
		print n
		print elevation_progress_modified(600000,0,n,ytd)
		n = n+10000
	print elevation_progress(600000,ytd)
	
	#print '-------------------------'
	#print ytd
	#print '-------------------------'
	f.close()

main()
