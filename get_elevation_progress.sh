#!/bin/sh

ath_id="942173"
curl https://www.strava.com/athletes/$ath_id > $ath_id.html
python ~/bike_stuff/elevation_progress.py $ath_id
rm $ath_id.html
